#!/usr/bin/env bash
clear

echo -e "\e[7m1/4 BUILD:\e[27m\n" &&

yarn build &&


echo -e "\n\n\e[7m2/4 GIT COMMIT:\e[27m\n" &&

git add -A &&
git commit -am "deploy react" &&


echo -e "\n\n\e[7m3/4 GIT PUSH:\e[27m\n" &&

git push &&


echo -e "\n\n\e[7m4/4 GIT PULL:\e[27m\n" &&

curl clio.local/pull.php