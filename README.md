Repo tools
===



Opportunities:
---
- [x] track available branches in your repos
- [x] switch multiple projects to selected branch
- [x] create pipelines to see the current task log
- [x] multiple instances of _repo tool_ for multiple users on your development server
- [x] simple UI

Setup:
---
1. Clone the repo
2. Setup your webserver to serve the `build` directory
3. [Setup your webserver to serve the /api path](https://symfony.com/doc/current/setup/web_server_configuration.html)
(change `/` in tutorial to `/api`)

- [ ] ...more steps...



_If you found a bug, please [open an issue](https://gitlab.com/nmish2005/repo-tools/issues/new)_