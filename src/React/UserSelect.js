import React from 'react';

import {withStyles} from "@material-ui/core";
import {Cancel as CancelIcon, Add as AddIcon} from '@material-ui/icons';
import {
    Chip,
    Fab,
    DialogActions,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField
} from "@material-ui/core";

import clsx from "clsx";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {selectUser, unselectUser} from "./redux/actions";
import fetchUsersAction from "./redux/actions/fetchUsers";
import {REWRITE_API_PATH} from "./redux/constants";


class UserSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {alertOpen: false};

        this.handleUserAdd = this.handleUserAdd.bind(this);
    }

    handleSelect(i) {
        this.props.selectUser(i);
        this.props.selectHandler();
    }

    handleDelete(i) {
        fetch(REWRITE_API_PATH + '/api/users/' + i, {
            method: 'DELETE'
        })
            .then(this.props.fetchUsers);

        if (i === this.props.selectedUser)
            this.props.unselectUser();
    }

    handleUserAdd() {
        let name = document.querySelector('#name').value;

        this.setState({alertOpen: false});

        fetch(REWRITE_API_PATH + '/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/ld+json'
            },
            body: "{\"id\": \"" + name + "\"}"
        })
            .then(this.props.fetchUsers)
            .catch(() => {
                alert('Error: user does not exist on a server.');
            });
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                {this.props.users && this.props.users.map((i, index) => (
                    <Chip
                        label={i}
                        key={index}
                        clickable
                        onDelete={() => this.handleDelete(i)}
                        onClick={() => this.handleSelect(i)}
                        className={classes.chip}
                        deleteIcon={<CancelIcon/>}
                        color={this.props.selectedUser === i ? 'primary' : undefined}
                    />
                ))}
                <Fab size="small" color="secondary" aria-label="Add" className={clsx(classes.fab, classes.alignRight)}
                     onClick={() => this.setState({alertOpen: true})}>
                    <AddIcon/>
                </Fab>
                <Dialog open={this.state.alertOpen} onClose={() => this.setState({alertOpen: false})}
                        aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add user</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Enter username to add:
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Username"
                            type="text"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({alertOpen: false})} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleUserAdd} color="primary">
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = state => state;

function mapDispatchToProps(dispatch) {
    return {
        selectUser: user => dispatch(selectUser(user)),
        unselectUser: () => dispatch(unselectUser()),
        fetchUsers: bindActionCreators(fetchUsersAction, dispatch)
    };
}


const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'left',
        flexWrap: 'wrap',
        padding: theme.spacing(3, 2)
    },
    chip: {
        margin: theme.spacing(1)
        // padding: theme.spacing(1)
    },
    fab: {
        margin: theme.spacing(1),
        marginTop: 3
    },
    alignRight: {
        marginRight: 12,
        marginLeft: 'auto'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserSelect));