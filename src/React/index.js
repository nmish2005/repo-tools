import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from "react-redux";

import App from './App';
import store from "./redux/store";

console.info('Hardcoded:\n\t- Repo paths\n\t- Branch suggestions');

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , window.reactroot);