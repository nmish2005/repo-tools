import React from 'react';

import {
    makeStyles,
    useTheme,
    withStyles,

    Select as MUISelect,

    Typography,
    TextField,
    MenuItem,
    Chip,
    Paper,
    Grid,
    Fab,
    FormGroup,
    FormControl,
    FormControlLabel,
    InputLabel,
    Checkbox,
    Divider,
    Container,
    Tooltip,
    Link,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary
} from "@material-ui/core";
import {Cancel as CancelIcon, SwapVert as SwitchIcon, ExpandMore as ExpandMoreIcon} from "@material-ui/icons";
import {emphasize} from "@material-ui/core/styles";

import Select from "react-select/creatable";

import {connect} from "react-redux";
import {bindActionCreators} from 'redux';

import moment from "moment";
import clsx from 'clsx';

import fetchInfoAction from './redux/actions/fetchInfo';
import {setCtx, setLang, toggleKeys, setBranches, toggleReset, unselectUser} from "./redux/actions";
import {REWRITE_API_PATH} from "./redux/constants";


class GitStats extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            panelExpanded: false,
            pipeline: {}
        };

        this.fetchStatus = this.fetchStatus.bind(this);
        this.switchBranches = this.switchBranches.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        // If there is no info in store for selected user
        if (nextProps.selectedUser && this.props.info !== undefined && !this.props.info[nextProps.selectedUser] && !nextProps.pending && !nextProps.error) {
            this.props.fetchInfo();
        }
    }

    componentDidMount() {
        if (!this.state.pipeline.status)
            this.fetchStatus();
    }

    fetchStatus(){
        fetch(REWRITE_API_PATH + '/api/status', {
            headers: {
                Accept: 'application/json'
            }
        })
            .then(r => r.json())
            .then(r => {
                r = r[0];
                r.logs = r.logs.replace('\n', '<br />');

                this.setState({pipeline: r})
            });
    }

    switchBranches() {
        let data = this.props.branches.slice();

        data.reverse();
        data = data.map(i => i.value);
        data = {
            user: this.props.selectedUser,
            reset: this.props.reset,
            prefetch: this.props.prefetch,
            branches: data,
            lang: this.props.language,
            ctx: this.props.context,
            updKeys: this.props.updKeys
        };
        data = JSON.stringify(data);

        this.setState({panelExpanded: true});

        fetch(REWRITE_API_PATH + '/api/pipeline', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data
        })
            .then(() => {
                setTimeout(() => {
                    this.fetchStatus();
                }, 5000)
            });
    }

    render() {
        const {classes} = this.props;

        return (
            <div>
                <div className={classes.withPadding}>
                    <IntegrationReactSelect/>
                    <FormGroup row>
                        <FormControlLabel
                            control={
                                <Checkbox checked={this.props.reset} onChange={this.props.toggleReset}/>
                            }
                            labelPlacement={'start'}
                            label="Reset"
                        />
                    </FormGroup>
                    <FormGroup row>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="ctx">Context</InputLabel>
                            <MUISelect
                                value={this.props.context}
                                onChange={this.props.setCtx}
                                inputProps={{
                                    id: 'ctx',
                                }}
                                className={classes.selectEmpty}
                            >
                                <MenuItem selected value='null'>
                                    Leave unchanged
                                </MenuItem>
                                <MenuItem value='dk'>DK</MenuItem>
                                <MenuItem value='sv'>SV</MenuItem>
                            </MUISelect>
                        </FormControl>

                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="lang">Language</InputLabel>
                            <MUISelect
                                value={this.props.language}
                                onChange={this.props.setLang}
                                inputProps={{
                                    id: 'lang',
                                }}
                                className={classes.selectEmpty}
                            >
                                <MenuItem value={'null'}>
                                    Leave unchanged
                                </MenuItem>
                                <MenuItem value='en'>EN</MenuItem>
                                <MenuItem value='dk'>DK</MenuItem>
                                <MenuItem value='sv'>SV</MenuItem>
                            </MUISelect>
                        </FormControl>

                        <FormControlLabel
                            control={
                                <Checkbox checked={this.props.updKeys} onChange={this.props.toggleKeys}/>
                            }
                            labelPlacement={'start'}
                            label="Update keys"
                        />
                    </FormGroup>

                    <Fab variant="extended" color='secondary' className={clsx(classes.fab, classes.alignRight)}
                         style={{display: "flex"}}
                         onClick={this.switchBranches}>
                        <SwitchIcon className={classes.extendedIcon}/>
                        Process
                    </Fab>

                    <ExpansionPanel expanded={this.state.panelExpanded} onChange={() => { this.setState({panelExpanded: !this.state.panelExpanded}) }}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography className={classes.heading}>{this.state.pipelineInProcess ? 'Pipeline in progress...' : 'Show last pipeline info'}</Typography>
                            <Typography className={classes.secondaryHeading}>Status: {this.state.pipeline.status}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Typography dangerouslySetInnerHTML={{__html: this.state.pipeline.logs}}>
                                {/*Already on 'master'<br />*/}
                                {/*Your branch is up-to-date with 'origin/master'.<br />*/}
                                {/*Switched to branch 'master'<br />*/}
                                {/*Your branch is up-to-date with 'origin/master'.<br />*/}
                                {/*Switched to branch 'master'<br />*/}
                                {/*Your branch is up-to-date with 'origin/master'.<br />*/}
                                {/*Switched to branch 'master'<br />*/}
                                {/*Your branch is up-to-date with 'origin/master'.<br />*/}
                                {/*Already on 'master'<br />*/}
                                {/*Your branch is up-to-date with 'origin/master'.*/}
                                {/*{this.state.pipelineLog}*/}
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                    <Divider style={{marginTop: 15}}/>

                    {this.props.info[this.props.selectedUser] && <Container>
                        <Typography variant={'body1'} style={{padding: '12px 0'}}>
                            Actual
                            on {moment(this.props.info[this.props.selectedUser].timestamp).format('DD/MM HH:mm:ss')}
                            <Link
                                href={'#'} className={classes.link}>
                                Refresh
                            </Link>.
                            Selected branches: {this.props.info[this.props.selectedUser].lastSwitch.map((branch, i) => (
                            <Typography
                                variant={"button"}>{branch}{this.props.info[this.props.selectedUser].lastSwitch[i + 1] && ", "}</Typography>
                        ))}
                        </Typography>
                    </Container>
                    }

                    <Divider/>
                    {this.props.info[this.props.selectedUser] && window.Object.keys(this.props.info[this.props.selectedUser].groups).map((group, key) => [
                        <div key={key} className={classes.marginTop}>
                            {/* Main */}
                            <Typography variant={'h5'} component={'h5'}
                                        style={{marginBottom: 12}}>{group.capitalize()}</Typography>

                            {/* Repo card */}
                            {window.Object.keys(this.props.info[this.props.selectedUser].groups[group]).map((repo, i) => (
                                this.props.info[this.props.selectedUser].lastSwitch.includes(repo) && (
                                    <div key={i}>
                                        <Typography variant={'h6'} style={{marginBottom: 10}}>
                                            {repo}
                                        </Typography>
                                        {/*big card*/}
                                        <Grid container spacing={3}>
                                            {window.Object.keys(this.props.info[this.props.selectedUser].groups[group][repo]).map((name, ii) => (
                                                <Grid item xs={4} key={ii}>
                                                    <Paper className={clsx(classes.paper, classes.active)}>
                                                        <Typography variant={'body1'}
                                                                    className={classes.bold}>{name}</Typography>
                                                    </Paper>
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </div>
                                )
                            ))}
                            <Grid container spacing={3}>
                                {window.Object.keys(this.props.info[this.props.selectedUser].groups[group]).map((repo, i) => (
                                    !this.props.info[this.props.selectedUser].lastSwitch.includes(repo) && (
                                        //small card
                                        <Tooltip
                                            title={
                                                <Typography variant={'body1'}
                                                            dangerouslySetInnerHTML={{__html: window.Object.keys(this.props.info[this.props.selectedUser].groups[group][repo]).join("<br />")}}/>
                                            }
                                            placement={'left'}
                                        >
                                            <Grid item xs={3} key={i}>
                                                <Paper className={classes.paper}>
                                                    <Grid container>
                                                        <Grid item xs={8}>
                                                            <Typography variant={'body2'}>{repo}</Typography>
                                                        </Grid>
                                                        <Grid item xs={4}>
                                                            <Typography
                                                                variant={'body2'}>{window.Object.keys(this.props.info[this.props.selectedUser].groups[group][repo]).length}</Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </Grid>
                                        </Tooltip>
                                    )
                                ))}
                            </Grid>
                        </div>
                    ])}

                </div>
            </div>
        );
    }
}

window.String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1)
};

const styles = theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary
    },
    bold: {
        fontWeight: 500
    },
    active: {
        background: theme.palette.primary.main,
        color: '#fff'
    },
    marginTop: {
        marginTop: theme.spacing(4)
    },
    withPadding: {
        padding: theme.spacing(3, 2)
    },
    borderLeft: {
        borderLeft: '1px solid #ccc'
    },
    fab: {
        margin: theme.spacing(1),
        height: theme.spacing(5)
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
        width: 160
    },
    link: {
        margin: theme.spacing(.5)
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0
    },
    alignRight: {
        marginRight: 12,
        marginLeft: 'auto'
    }
});


const mapStateToProps = state => state;

function mapDispatchToProps(dispatch) {
    // noinspection JSUnresolvedFunction
    return {
        unselectUser: () => dispatch(unselectUser()),
        setBranches: e => dispatch(setBranches(e)),
        toggleReset: () => dispatch(toggleReset()),
        toggleKeys: () => dispatch(toggleKeys()),
        setLang: e => dispatch(setLang(e.target.value)),
        setCtx: e => dispatch(setCtx(e.target.value)),
        fetchInfo: bindActionCreators(fetchInfoAction, dispatch)
    };
}

//Multiple select stuff
const suggestions = [
    'master',
    'production',
    'CP-230',
    'UN-130',
    'UN-135'
].map(suggestion => ({
    value: suggestion,
    label: suggestion,
}));

const useStyles = makeStyles(theme => ({
    input: {
        display: 'flex',
        padding: 0,
        height: 'auto',
    },
    alignRight: {
        marginLeft: "auto",
        marginRight: -12
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    chip: {
        margin: theme.spacing(0.5, 0.25),
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08,
        ),
    },
    noOptionsMessage: {
        padding: theme.spacing(1, 2),
    },
    placeholder: {
        position: 'absolute',
        left: 2,
        bottom: 6,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
}));

function NoOptionsMessage(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function inputComponent({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

function Control(props) {
    const {
        children,
        innerProps,
        innerRef,
        selectProps: {classes, TextFieldProps},
    } = props;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps,
                },
            }}
            {...TextFieldProps}
        />
    );
}

function Option(props) {
    return (
        <MenuItem
            ref={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontWeight: props.isSelected ? 500 : 400,
            }}
            {...props.innerProps}
        >
            {props.children}
        </MenuItem>
    );
}

function Placeholder(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function ValueContainer(props) {
    return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
    return (
        <Chip
            tabIndex={-1}
            label={props.children}
            className={clsx(props.selectProps.classes.chip, {
                [props.selectProps.classes.chipFocused]: props.isFocused,
            })}
            color={'primary'}
            onDelete={props.removeProps.onClick}
            deleteIcon={<CancelIcon {...props.removeProps} />}
        />
    );
}

function Menu(props) {
    return (
        <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
}

const components = {
    Control,
    Menu,
    MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    ValueContainer
};

function IntegrationReactSelect(props) {
    const classes = useStyles();
    const theme = useTheme();

    function handleChangeMulti(value) {
        props.setBranches(value);
    }

    const selectStyles = {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
    };

    return (
        <Select
            classes={classes}
            styles={selectStyles}
            inputId="react-select-multiple"
            TextFieldProps={{
                label: 'Available branches',
                InputLabelProps: {
                    htmlFor: 'react-select-multiple',
                    shrink: true,
                },
            }}
            placeholder="Select branches to checkout"
            options={suggestions}
            components={components}
            value={props.branches}
            onChange={handleChangeMulti}
            isMulti
        />
    );
}

// eslint-disable-next-line no-func-assign
IntegrationReactSelect = connect(mapStateToProps, mapDispatchToProps)(IntegrationReactSelect);
/**/

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(GitStats));