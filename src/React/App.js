import React from 'react';

import {Paper, Slide, Snackbar} from '@material-ui/core';
import {createMuiTheme, withStyles, MuiThemeProvider as ThemeProvider} from '@material-ui/core/styles';
import {blue, deepPurple} from "@material-ui/core/colors";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import UserSelect from "./UserSelect";
import GitStats from "./GitStats";
import fetchUsersAction from "./redux/actions/fetchUsers";


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            snackOpen: false
        };

        this.selectUser = this.selectUser.bind(this);
    }

    componentDidMount() {
        this.props.fetchUsers();
    }

    selectUser() {
        this.setState({snackOpen: true});

        setTimeout(() => {
            this.setState({snackOpen: false})
        }, 1000);
    }

    render() {
        const {classes} = this.props;

        return (
            <ThemeProvider theme={theme}>
                <div className={classes.root}>
                    <Snackbar
                        open={this.state.snackOpen}
                        onClose={() => this.setState({snackOpen: false})}
                        TransitionComponent={props => <Slide {...props} direction="up"/>}
                        ContentProps={{
                            'aria-describedby': 'userSelected_snackbar',
                        }}
                        anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
                        message={<span id="userSelected_snackbar">Selected user <b>{this.props.selectedUser}</b>. Loading repo list...</span>}
                    />

                    <Paper className={classes.paper}>
                        <UserSelect selectHandler={this.selectUser}/>
                        <GitStats/>
                    </Paper>
                </div>
            </ThemeProvider>
        );
    }
}

const styles = () => ({
    root: {
        padding: 24,
        display: 'flex',
        justifyContent: 'center'
    },
    paper: {
        width: '70vw'
    }
});

const theme = createMuiTheme({
    palette: {
        primary: {main: blue.A400},
        secondary: deepPurple,
    },
    overrides: {
        MuiTooltip: {
            tooltip: {
                backgroundColor: '#f5f5f9',
                color: 'rgba(0, 0, 0, 0.87)',
                maxWidth: 220,
                fontSize: 12,
                border: '1px solid #dadde9',
            }
        }
    }
});

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchUsers: fetchUsersAction
}, dispatch);

const mapStateToProps = state => state;

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(App));