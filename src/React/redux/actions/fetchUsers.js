import {fetchUsersPending, fetchUsersSuccess, fetchUsersError} from './index';
import {REWRITE_API_PATH} from "../constants";

function fetchUsers() {
    return dispatch => {
        dispatch(fetchUsersPending());
        fetch(REWRITE_API_PATH + '/api/users', {
            headers: {
                Accept: 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {

                res = res.map(i => i.id);

                dispatch(fetchUsersSuccess(res));

                return res;
            })
            .catch(error => {
                dispatch(fetchUsersError(error));
            })
    }
}

export default fetchUsers;