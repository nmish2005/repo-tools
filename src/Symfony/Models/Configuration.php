<?php

declare(strict_types=1);

namespace App\Models;

use Symfony\Component\Yaml\Yaml;

class Configuration
{
    const CONFIG_PATH = "/runner/config.yml";

    const SNAPSHOT_PATH = "/runner/snapshot.json";

    const HOMEDIR_PATH = "/home/";

    const STAGE_GIT = 0;

    const STAGE_CONTEXT = 1;

    const STAGE_LANGUAGE = 2;

    const STAGE_TRANSLATION = 3;

    const STAGE_YAKINTOS_LINK = 4;

    const STAGE_YAKINTOS_BUILD = 5;

    const STAGE_CACHE = 6;

    const STAGE_INFO = 7;

    const STAGE_NAMES = [
        'Git reset, fetch, checkout',
        'Switch context',
        'Switch language',
        'Switch translation',
        'Yakintos link',
        'Yakintos build, watch',
        'Flow flush cache',
        'Gathering information'
    ];

    static function getConfiguration() :array
    {
        $config = file_exists($_SERVER['DOCUMENT_ROOT'] . "/.." . self::CONFIG_PATH) ?
            file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/.." . self::CONFIG_PATH) :
            '';

        $config = Yaml::parse($config);

        return (array) $config;
    }

    static function setConfig(array $data) :void
    {
        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . "/.." . self::CONFIG_PATH,
            empty($data) ?
                '' :
                Yaml::dump($data));
    }
}