<?php

namespace App\Models;

class ReposIterator implements \Iterator
{
    protected $storage = [];

    public function set($key)
    {
        $this->storage = $key;
    }

    public function get($key)
    {
        return $this->storage[$key];
    }

    public function current()
    {
        return new Repository(current($this->storage));
    }

    public function key()
    {
        return key($this->storage);
    }

    public function next(): void
    {
        next($this->storage);
    }

    public function rewind(): void
    {
        reset($this->storage);
    }

    public function valid(): bool
    {
        return null !== key($this->storage);
    }
}