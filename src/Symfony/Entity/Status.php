<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * @ApiResource(
 *     collectionOperations={"get"={"method"="GET", "path"="/status"}},
 *     itemOperations={"get"={"method"="GET", "path"="/status/{id}"}}
 * )
 */
class Status
{
    const READY = 'ready';

    const PENDING = 'pending';

    const INACTIVE = 'inactive';

    const SNAPSHOT_PATH = '/../runner/state_snapshot.json';

    /**
     * @ApiProperty(identifier=true)
     */
    private $status;

    /**
     * @ApiProperty(identifier=true)
     */
    private $logs;

    /**
     * @ApiProperty(identifier=true)
     */
    private $snapshot;

    /**
     * @return mixed
     */
    public function getLogs() {
        return $this->logs;
    }

    /**
     * @return mixed
     */
    public function getSnapshot() {
        return $this->snapshot;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    public function __construct() {
        $this->status = self::getState();
        $this->logs = Pipeline::getLogs($this->status);
        $this->snapshot = file_exists($_SERVER['DOCUMENT_ROOT'] . self::SNAPSHOT_PATH) ?
            file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::SNAPSHOT_PATH) :
            json_encode([]);
    }

    /**
     * Returns state and runner logs
     * @return string
     */
    static function getState() {
        // ready
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . Pipeline::PIPELINE_PATH) &&
            !file_exists($_SERVER['DOCUMENT_ROOT'] . Pipeline::PENDING_PIPELINE_PATH))
            return self::READY;


        // pending
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . Pipeline::PENDING_PIPELINE_PATH))
            return self::PENDING;

        // inactive
        return self::INACTIVE;
    }
}