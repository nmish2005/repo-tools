<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator\Constraints as AppAssert;

/**
 * @ApiResource()
 */
class Users
{
    /**
     * @ApiProperty(identifier=true)
     * @AppAssert\ExistedUser
     */
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}
