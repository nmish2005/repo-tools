<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator\Constraints as AppAssert;

/**
 * @ApiResource(
 *     collectionOperations={"post"={"method"="POST", "path"="/pipeline"}},
 *     itemOperations={"get"={"method"="GET", "path"="/pipeline/{id}"}}
 * )
 */
class Pipeline
{
    const PIPELINE_PATH = '/../runner/pipeline.dat';

    const DONE_PIPELINE_PATH = '/../runner/done_pipeline.log';

    const PENDING_PIPELINE_PATH = '/../runner/pending_pipeline.log';

    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @ApiProperty
     */
    private $branches;

    /**
     * @ApiProperty
     * @AppAssert\NoPendingPipeline
     */
    private $collectStatus;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function __construct($branches = '', $collectStatus = true) {
        $this->branches = $branches;
        $this->collectStatus = $collectStatus;
    }

    /**
     * Returns current or previous pipeline logs
     * @return string
     */
    static function getLogs($status = Status::READY) {

        switch ($status) {
            case Status::READY:
                return file_exists($_SERVER['DOCUMENT_ROOT'] . self::DONE_PIPELINE_PATH) ?
                    file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::DONE_PIPELINE_PATH) :
                    '';

            case Status::PENDING:
                return file_exists($_SERVER['DOCUMENT_ROOT'] . self::PENDING_PIPELINE_PATH) ?
                    file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::PENDING_PIPELINE_PATH) :
                    '';
        }

        return '';
    }
}