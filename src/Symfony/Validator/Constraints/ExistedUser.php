<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExistedUser extends Constraint
{
    public $message = 'User {{ string }} does not exist';
}