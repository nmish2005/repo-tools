<?php

namespace App\Validator\Constraints;

use App\Entity\Pipeline;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class NoPendingPipelineValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NoPendingPipeline) {
            throw new UnexpectedTypeException($constraint, NoPendingPipeline::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_bool($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'bool');

            // separate multiple types using pipes
            // throw new UnexpectedValueException($value, 'string|int');
        }

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . Pipeline::PIPELINE_PATH)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}