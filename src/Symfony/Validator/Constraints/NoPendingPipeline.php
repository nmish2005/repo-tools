<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NoPendingPipeline extends Constraint
{
    public $message = 'You can\'t create new pipeline before previous is done';
}