<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Users;
use App\Models\Configuration;

class UsersCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function getCollection(string $resourceClass, string $operationName = null)
    {
        $conf = Configuration::getConfiguration();

        if (empty($conf['users']))
            return null;

        foreach ($conf['users'] as $user)
            yield new Users($user);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Users::class === $resourceClass;
    }
}