<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Users;
use App\Models\Configuration;

class UsersItemDataPersister implements DataPersisterInterface
{
    public function supports($data): bool
    {
        return $data instanceof Users;
    }

    public function persist($data)
    {
        $conf = Configuration::getConfiguration();

        if (empty($conf['users']))
            $conf['users'] = [];

        $conf = array_merge_recursive(
            $conf,
            [
                'users' => strtolower($data->getId())
            ]
        );

        $conf['users'] = array_values(
            array_unique($conf['users'])
        );

        Configuration::setConfig($conf);
    }

    public function remove($data)
    {
        $conf = Configuration::getConfiguration();

        if (in_array($data->getId(), $conf['users']))
            unset($conf['users'][array_search($data->getId(), $conf['users'])]);

        if (empty($conf['users']))
            unset($conf['users']);
        else
            $conf['users'] = array_values($conf['users']);

        Configuration::setConfig($conf);
    }
}