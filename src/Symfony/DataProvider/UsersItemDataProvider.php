<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Users;
use App\Models\Configuration;

class UsersItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $conf = Configuration::getConfiguration();

        return !empty($conf['users']) && in_array($id, $conf['users']) ?
            new Users($id) :
            null;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Users::class === $resourceClass;
    }
}