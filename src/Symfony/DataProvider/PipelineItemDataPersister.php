<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Pipeline;
use App\Models\Configuration;

class PipelineItemDataPersister implements DataPersisterInterface
{
    public function supports($data): bool
    {
        return $data instanceof Pipeline;
    }

    public function persist($data)
    {
        $commands = '';

        // iteration through stages
        foreach (Configuration::STAGE_NAMES as $stageKey => $stageName)
        {
            $echo = "echo Stage: {$stageName}... ;";

            switch ($stageKey)
            {
                case Configuration::STAGE_GIT:
                    break;

                case Configuration::STAGE_CONTEXT:
                    break;

                case Configuration::STAGE_LANGUAGE:
                    break;

                case Configuration::STAGE_TRANSLATION:
                    break;

                case Configuration::STAGE_YAKINTOS_LINK:
                    break;

                case Configuration::STAGE_YAKINTOS_BUILD:
                    break;

                case Configuration::STAGE_CACHE:
                    break;

                case Configuration::STAGE_INFO:
                    $commands .= $echo . "./bin/console app:gather-info\n";
                    break;
            }
        }

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . Pipeline::PIPELINE_PATH, $commands);
    }

    public function remove($data)
    {
    }
}