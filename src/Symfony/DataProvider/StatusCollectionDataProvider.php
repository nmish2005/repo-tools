<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Status;

class StatusCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function getCollection(string $resourceClass, string $operationName = null) {
        yield new Status();
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool {
        return Status::class === $resourceClass;
    }
}