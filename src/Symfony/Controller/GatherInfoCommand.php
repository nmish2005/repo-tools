<?php

declare(strict_types=1);

namespace App\Controller;

use App\Models\Configuration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GatherInfoCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:gather-info';

    protected function configure()
    {
        $this
            ->setDescription('Gathers info about repos branches, library links etc.')
            ->setHelp('This command executes \'git status\' command for each repo from config/repos.yml ' .
                'and stores results to snapshot.json');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groups = [];

        $snapshot = file_exists(getcwd() . Configuration::SNAPSHOT_PATH) ?
            json_decode(
                file_get_contents(getcwd() . Configuration::SNAPSHOT_PATH) ?: "",
                true) :
            [];

        // iterate through all repos
        $configuration = Configuration::getConfiguration();

        if (!empty($configuration['groups']))
        {
            foreach ($configuration['groups'] as $groupName => $repos)
            {
                foreach ($repos as $repoName => $repoData)
                {
                    $output->writeln($groupName . " -> " . $repoName . " -> " . $repoData['path']);
                }
            }
        }

        $snapshot['groups'] = $groups;

        file_put_contents(getcwd() . Configuration::SNAPSHOT_PATH, json_encode($snapshot, JSON_PRETTY_PRINT));

        $output->writeln("Done\n");
    }
}