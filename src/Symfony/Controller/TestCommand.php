<?php

declare(strict_types=1);

namespace App\Controller;

use App\Models\Configuration;
use App\Models\ReposIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:testcmd';

    protected function configure()
    {
        $this
            ->setDescription('Gathers info about repos branches, library links etc.')
            ->setHelp('This command executes \'git status\' command for each repo from config/repos.yml ' .
                'and stores results to snapshot.json');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $itr = new ReposIterator();

        $itr->set(['a', 'b', 'c']);

        foreach ($itr as $val)
            $output->writeln($val->isJSProject());
    }
}