#!/bin/sh

cd ..

doPipeline()
{
  # prevent processing pipeline if there one in process
  if [ -f ./runner/pending_pipeline.log ]; then

    echo "[error] there is already working runner (if it isn't true remove pending_pipeline.log) \n"

    exit 1

  fi

  # if there is a pending pipeline
  if [ -f ./runner/pipeline.dat ]; then

    # executing pipeline
    instructions=`cat ./runner/pipeline.dat`

    eval "$instructions" > ./runner/pending_pipeline.log

    # removing after work is done
    mv ./runner/pending_pipeline.log ./runner/done_pipeline.log

    rm ./runner/pipeline.dat

    echo "[info] pipeline finished. See details in done_pipeline.log\n"

  fi
}

loop()
{
  doPipeline

  sleep 5

  loop
}

echo "[ok] runner successfully started\n"

loop