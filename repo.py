#!/usr/bin/env python
import os
import copy
import time
from multiprocessing import Pool
from itertools import takewhile, imap, ifilter
from operator import add
from src.repo.arguments import arguments
from src.repo.output import print_result, print_start_execution, print_progress
from src.repo.repository_list import get_repo_paths, path_type
from src.repo.commands import execute_command, create_commands
from src.repo.versioning import get_package_version
import pprint


def execute_next_command(commands, arguments):
    """Execute a command and return an iterator with an output."""

    return ifilter(lambda output: output is not None,
                   imap(lambda command: execute_command(command, arguments), commands))


def get_next_output(commands, arguments):
    """Returns next output.

    It will be executing commands till the moments when result is available.
    """

    for output in execute_next_command(commands, arguments):
        yield output
        if output['status'] == 'error':
            break


def _prepare_arguments_for_repo(path, arguments):
    """Prefilter arguments per each repository."""

    new_arguments = copy.deepcopy(arguments)
    exclude_list_for_libraries = ['development', 'master']
    if new_arguments.switch is not None:
        if path_type(path) == 'application' and new_arguments.switch[0] != 'development':
            # use 'development' as default
            new_arguments.switch.insert(0, 'development')

        if path_type(path) == 'library':
            # delete all 'development' branch in params
            new_arguments.switch = [x for x in new_arguments.switch if x not in exclude_list_for_libraries]
            # use tag version as default
            new_arguments.switch.insert(0, get_package_version(path))

        if path_type(path) == 'standalone_library':
            # delete all 'development' branch in params
            new_arguments.switch = [x for x in new_arguments.switch if x != 'development']
            # use 'master' as default
            new_arguments.switch.insert(0, 'master')

    # never merge 'development' for libraries
    if new_arguments.merge == 'development' and path_type(path) in ['library', 'standalone_library']:
        new_arguments.merge = None

    return new_arguments


def execute_commands_per_repo(path, arguments):
    """Execute commands one by one for repository and return a result tulip(path, [output, output, ...]) after all."""

    if not os.path.exists(path) or not os.path.isdir(path):
        result = 'Dir doesn\'t exist'
        return path, [dict(path=path, status='error', text=result)]

    repo_arguments = _prepare_arguments_for_repo(path, arguments)
    repo_commands = create_commands(repo_arguments)
    os.chdir(path)

    return path, reduce(lambda results, output: add(results, [output]),
                        get_next_output(repo_commands, repo_arguments),
                        list())


def generate_process_status_list(repo_paths):
    return map(lambda path: dict(path=path, status='progress'), repo_paths)


def get_process_status(async_result):
    """Return a status of a process."""

    if async_result.ready():
        path, results = async_result.get()

        if list(takewhile(lambda result: result['status'] == 'error', reversed(results))):
            return 'error'
        else:
            return 'done'
    else:
        return 'progress'


def wait_while_in_progress(async_results):
    """The main loop for execution of all commands in all repositories."""

    time.sleep(0.1)
    process_status_list = map(lambda (path, async_result): dict(path=path, status=get_process_status(async_result)),
                              async_results)

    print_progress(process_status_list)
    if filter(lambda process: process['status'] == 'progress', process_status_list):
        return wait_while_in_progress(async_results)
    else:
        return map(lambda (path, async_result): async_result.get(), async_results)


def get_async_result(p, path, arguments):
    return p.apply_async(execute_commands_per_repo, (path, arguments))


def get_async_results(arguments, paths):
    p = Pool(len(paths))
    async_results = map(lambda path: (path, get_async_result(p, path, arguments)), paths)
    p.close()
    return async_results


def execute_commands(arguments):
    """Execute commands for all repositories simultaneously."""

    repo_paths = get_repo_paths(arguments.path_filter)
    print_start_execution(create_commands(arguments), generate_process_status_list(repo_paths))
    return print_result(wait_while_in_progress(get_async_results(arguments, repo_paths)))


if __name__ == "__main__":
    if len(create_commands(arguments)) < 2:
        print('Nothing to execute. Use -h argument to see all available options')
    else:
        exit(execute_commands(arguments))
